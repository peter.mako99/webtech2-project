var express = require('express');
var router = express.Router();
const { Task } = require('../database/models/task.model');
const { User } = require('../database/models/user.model');

const jwt = require('jsonwebtoken');

// check whether the request has a valid JWT access token
let authenticate = (req, res, next) => {
    let token = req.header('x-access-token');

    // verify the JWT
    jwt.verify(token, User.getJWTSecret(), (err, decoded) => {
        if (err) {
            // there was an error
            // jwt is invalid - * DO NOT AUTHENTICATE *
            res.status(401).send(err);
        } else {
            // jwt is valid
            req.user_id = decoded._id;
            next();
        }
    });
}

// Get all tasks

router.get('/tasks', authenticate, (req, res) => {
    //auth.authenticate(req, res, next);
    Task.find({}).then((tasks) => {
        res.send(tasks);
    })
});

//Get single task

router.get('/task/:id',(req, res) => { 
    
    Task.findOne({
        _id: req.params.id
    }).then((singleTask) => {
        res.send(singleTask);
    })
});


// Save Task

router.post('/tasks', authenticate, (req, res) => {
    let title = req.body.title;
    let time = req.body.time;
    let priority = req.body.priority;
    let content = req.body.content;
    let date = req.body.date;

    let newTask = new Task({
        title,
        time,
        priority,
        content,
        date
    });
    newTask.save().then((taskDoc) => {
        res.send(taskDoc);
    });

});

// Delete Task

router.delete('/task/:id', authenticate, (req, res) => {
    Task.findOneAndRemove({
        _id: req.params.id
    }).then((removedTaskDoc) => {
        res.send(removedTaskDoc);
    })
});

//Update Task

router.patch('/task/:id', authenticate, (req, res) => {
    let uTask = req.body;
    Task.findOneAndUpdate({ _id: req.params.id }, {
        $set: req.body
    }).then(() => {
        res.send(uTask);
    });
});

module.exports = router;