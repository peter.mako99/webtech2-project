var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next){ //request, response, next
    res.render('index.html');
});

module.exports = router;