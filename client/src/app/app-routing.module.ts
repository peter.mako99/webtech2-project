import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent } from './authentication/authentication.component';
import { RegistrationComponent } from './authentication/registration/registration.component';
import { TasksComponent } from './tasks/tasks.component';
import { HomeComponent } from './home/home.component';
import { AddTaskComponent } from './tasks/add-task/add-task.component';
import { AuthGuardGuard } from './auth-guard.guard';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: AuthenticationComponent },
  { path: 'register', component: RegistrationComponent},
  { path: 'tasks', component: TasksComponent, canActivate:[AuthGuardGuard]},
  { path: 'add_task', component: AddTaskComponent, canActivate:[AuthGuardGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
