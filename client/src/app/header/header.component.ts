import { HtmlParser } from '@angular/compiler';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from '../services/auth.service'


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private authService:AuthService) { }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    if (this.authService.getAccessToken() != null){
      document.getElementById("profile").innerHTML= "Sign Out";
      document.getElementById("profile").onclick = this.authService.logout;
    }
    else{
      document.getElementById("profile").innerHTML= "Sign In";
      document.getElementById("profile").onclick = this.authService.navToLogin;
    }
  }

}
