import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { User } from '../../../../database/models/user.model';
import { AuthService } from '../services/auth.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit(): void {
    if (this.authService.getAccessToken() != null){
      alert("You are already logged in!");
      this.router.navigate(['/tasks']);
    }
  }

  onLoginButtonClick(username: string, password:string){
      this.authService.login(username, password).subscribe((res: HttpResponse<any>) => {
          console.log(res);
      })
  }

}
