import { Component, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective,FormControl,Validators, NgForm } from '@angular/forms';
import { TaskService } from '../services/task.service';
import { Task } from '../../Task'
import { AuthService } from '../services/auth.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})

export class TasksComponent implements OnInit, AfterViewInit {
  tasks: Task[];
  matcher = new MyErrorStateMatcher();

  alterRecord = new FormGroup({
    id: new FormControl(),
    title: new FormControl(),
    priority: new FormControl(),
    time: new FormControl(),
    content: new FormControl(),
    date: new FormControl(),
  });

  priorities=[{
    name:'High',
  },{
    name:'Medium',
  },{
    name:'Low',
  }]

  displayedColumns: string[] = ['status', 'title', 'time', 'priority', 'date' , 'content', 'action'];
  dataSource: MatTableDataSource<Task>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private taskService:TaskService, private authService: AuthService, private fb:FormBuilder){
  }

  ngOnInit(): void {
    this.taskService.getTasks()
    .subscribe(tasks => {
      this.dataSource = new MatTableDataSource(tasks);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    this.alterRecord = this.fb.group({
      id: ['', Validators.required],
      priority: ['', Validators.required],
      title: ['', Validators.required],
      time: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      content: [''],
      date: ['', Validators.required]
		});

  }

  ngAfterViewInit() {
  }

  fillDialog(task) {
    document.getElementById("myModal").style.display = "block";
    var date = new Date(task.date);
    const toSelect = this.priorities.find(p => p.name == task.priority);

    this.alterRecord.get('id').setValue(task._id);
    this.alterRecord.get('title').setValue(task.title);
    this.alterRecord.get('time').setValue(task.time);
    this.alterRecord.get('content').setValue(task.content);
    this.alterRecord.get('date').setValue(date);
    this.alterRecord.get('priority').setValue(toSelect);
  }

  applyFilter(ev: Event) {
    const filterValue = (ev.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  deleteTask(id){
    var retVal = confirm("Are you sure, you want to delete this task?");
    if( retVal == true ) {
      this.taskService.deleteTask(id)
      .subscribe(data => {
        const index = this.dataSource.data.indexOf(id);
        this.dataSource.data.splice(index, 1);
        this.dataSource._updateChangeSubscription();
      });
    } else {
      alert("No changes were made.");
    }
  }

  updateStatus(task){
    var _task= {
      _id:task._id,
      title: task.title,
      isDone: !task.isDone,
    };

    this.taskService.updateStatus(_task)
      .subscribe(data => {
        task.isDone = !task.isDone;
      })
  }

  updateTask(event){
    var _task= {
      _id:this.alterRecord.get('id').value,
      title: this.alterRecord.get('title').value,
      priority: this.alterRecord.get('priority').value.name,
      content: this.alterRecord.get('content').value,
      date: this.alterRecord.get('date').value,
      time: this.alterRecord.get('time').value,
    };

    var retVal = confirm("Are you sure, you want to modify this task?");
    if( retVal == true ) {
      this.taskService.updateStatus(_task)
      .subscribe(data => {
        document.getElementById("myModal").style.display = "none";
        var index;
        for (var i = 0; i < this.dataSource.data.length; i++) {
          if(this.dataSource.data[i]._id==data._id){
            index=i;
          }
        }
        this.dataSource.data[index].title = data.title;
        this.dataSource.data[index].priority = data.priority;
        this.dataSource.data[index].content = data.content;
        this.dataSource.data[index].date = data.date;
        this.dataSource.data[index].time = data.time;
        this.dataSource._updateChangeSubscription();
      });
    } else {
      alert("No changes were made.");
    }
  }

  daysLeft(param: string, isDone:boolean){
    var today=new Date();
    var expiry = new Date(param);
    if (today.getMonth()==11 && today.getDate()>25)
    {
    expiry.setFullYear(expiry.getFullYear()+1);
    }
    var one_day=1000*60*60*24;
    var daysLeft = Math.ceil((expiry.getTime()-today.getTime())/(one_day))

    if(isDone){
      return "Completed"
    }
    else{
      if(daysLeft<0){
        return "Expired";
      }
      else{
        return daysLeft;
      }
    }
  }
}
