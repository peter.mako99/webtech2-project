import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective,FormControl,Validators, NgForm } from '@angular/forms';
import { Task } from 'src/Task';
import { TaskService } from '../../services/task.service'
import { AuthService } from '../../services/auth.service'
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {
  tasks: Task[];
  matcher = new MyErrorStateMatcher();

  alterRecord = new FormGroup({
    id: new FormControl(),
    title: new FormControl(),
    priority: new FormControl(),
    time: new FormControl(),
    content: new FormControl(),
    date: new FormControl(),
  });

  priorities=[{
    name:'High',
  },{
    name:'Medium',
  },{
    name:'Low',
  }]

  constructor(private taskService:TaskService, private authService: AuthService, private fb:FormBuilder) { }

  ngOnInit(){
    this.alterRecord = this.fb.group({
      id: ['', Validators.required],
      priority: ['', Validators.required],
      title: ['', Validators.required],
      time: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      content: [''],
      date: ['', Validators.required]
		});
  }

  addTask(event){
    var newTask= {
      _id:this.alterRecord.get('id').value,
      title: this.alterRecord.get('title').value,
      priority: this.alterRecord.get('priority').value.name,
      content: this.alterRecord.get('content').value,
      date: this.alterRecord.get('date').value,
      time: this.alterRecord.get('time').value,
    };

    var retVal = confirm("Are you sure, you want to add this task?");
    if( retVal == true ) {
      this.taskService.addTask(newTask)
      .subscribe(task => {
        alert("New task added, navigating to Taskview");
        this.authService.navToTasks();
      });
    }
    else {
      alert("No changes were made.");
    }
  }
}
