import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { map } from 'rxjs/operators';
import { Task } from '../../Task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  constructor(private httpClient:HttpClient){
    console.log('Task service online...');
   }

   getTasks(){
    return this.httpClient.get<Task[]>('http://localhost:3000/api/tasks')
    .pipe(
      map((res) => {
        return <Task[]> res
      })
    );
   }

   addTask(newTask){
     console.log(newTask);
      var headers = new HttpHeaders({'Content-Type': 'application/json'});
      return this.httpClient.post('http://localhost:3000/api/tasks', JSON.stringify(newTask), {headers: headers})
      .pipe(
        map((res) => {
          return <Task> res
        })
      );
   }

   deleteTask(id){
      return this.httpClient.delete('http://localhost:3000/api/task/'+id)
      .pipe(
        map((res) => {
          return <Task[]> res
        })
      );
   }

   updateStatus(task){
    var headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.patch('http://localhost:3000/api/task/'+task._id, JSON.stringify(task), {headers: headers})
    .pipe(
      map((res) => {
        return <Task> res
      })
    );
   }
}
