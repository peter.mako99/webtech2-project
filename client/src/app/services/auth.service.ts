import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse} from '@angular/common/http'
import { User } from '../../../../database/models/user.model';
import { shareReplay,tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable,throwError } from 'rxjs';
import { tokenize } from '@angular/compiler/src/ml_parser/lexer';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient:HttpClient, private router: Router){
    console.log('AuthService online...');
  }


  login(username: string, password:string){
      return this.httpClient.post('http://localhost:3000/api/users/login', {
        username,
        password
      }, { observe: 'response'}).pipe(
        shareReplay(),
        tap((res: HttpResponse<any>) => {
          this.setSession(res.body._id, res.headers.get('x-access-token'), res.headers.get('x-refresh-token'));
          alert("Successfully logged in!");
          this.router.navigate(['/tasks']).then(() => {
            window.location.reload();
          });
        }),
        catchError((error: HttpErrorResponse): Observable<any> => {
          // we expect 400, it's not a failure for us.
          if (error.status === 400) {
            alert("The credentials entered are incorrect, check again, or registed if you don't have an account!");
          }
          // other errors we don't know how to handle and throw them further.
          return throwError(error);
        },
      )
      )
  }

  register(email: string, username: string, password:string){
    return this.httpClient.post('http://localhost:3000/api/users', {
    email,
    username,
    password
    }, { observe: 'response'}).pipe(
      shareReplay(),
      tap((res: HttpResponse<any>) => {
        this.setSession(res.body._id, res.headers.get('x-access-token'), res.headers.get('x-refresh-token'));
        alert("Successfully signed up and logged in!");
        this.router.navigate(['/tasks']);
      }),
      catchError((error: HttpErrorResponse): Observable<any> => {
        // we expect 400, it's not a failure for us.
        if (error.status === 400) {
          alert("This E-mail, Username (or both) already exists in our system, try logging in instead!");
        }
        // other errors we don't know how to handle and throw them further.
        return throwError(error);
      },
    )
  )
}

  private setSession(userId: string, accessToken: string, refreshToken: string) {
    localStorage.setItem('user-id', userId);
    localStorage.setItem('x-access-token', accessToken);
    localStorage.setItem('x-refresh-token', refreshToken);
  }

  logout(){
    alert("Logged out!")
    localStorage.removeItem('user-id');
    localStorage.removeItem('x-access-token');
    localStorage.removeItem('x-refresh-token');
    this.router.navigate(['/login']);
  }

  getAccessToken(){
    return localStorage.getItem('x-access-token');
  }

  getRefreshToken(){
    return localStorage.getItem('x-refresh-token');
  }

  setAccessToken(accessToken: string){
    localStorage.setItem('x-access-token', accessToken)
  }

  getUserId(){
    return localStorage.getItem('user-id');
  }

  getNewAccessToken(){
    return this.httpClient.get("http://localhost:3000/api/users/me/access-token", {
      headers: {
        'x-refresh-token': this.getRefreshToken(),
        '_id': this.getUserId()
      },
      observe: 'response'
    }).pipe (
      tap((res: HttpResponse<any>) => {
        this.setAccessToken(res.headers.get('x-access-token'));
      })
    )
  }

  isLoggedIn(){
    if(this.getAccessToken()!=null){
      return true;
    }
    else{
      return false;
    }
  }

  navToTasks(){
    this.router.navigate(['/tasks']);
  }

  navToLogin(){
    this.router.navigate(['/login']);
  }
}
