export class Task{
  title: string;
  isDone: boolean;
  _id: string;
  time: number;
  priority: string;
  content: string;
  date: Date;
}
