const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 1,
        trim: true,
    },
    isDone: {
        type: Boolean,
        default: false
    },
    time: {
        type: Number,
        default: 0,
    },
    priority: {
        type: String,
        required: true,
        default: 'Low'
    },
    content: {
        type: String
    },
    date: {
        type: Date,
        required: true
    }
})

const Task = mongoose.model('Task', TaskSchema);

module.exports = { Task }